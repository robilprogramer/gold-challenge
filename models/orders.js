'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class orders extends Model {
    static associate(models) {
      // define association here
    }
  }
  orders.init({
    id_user: DataTypes.STRING,
    total_harga: DataTypes.NUMERIC,
    tanggal_pesanan: DataTypes.DATE,
    status_pesanan: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'orders',
  });
  return orders;
};