'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    static associate(models) {
      // define association here
    }
  }
  users.init({
    email: DataTypes.STRING,
    nama_lengkap: DataTypes.STRING,
    nomor_telepon: DataTypes.STRING,
    password: DataTypes.STRING,
    jenis_kelamin: DataTypes.STRING,
    tanggal_lahir: DataTypes.DATE,
    alamat: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'users',
  });
  return users;
};