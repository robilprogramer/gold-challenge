'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class orders_detail extends Model {
    static associate(models) {
      // define association here
    }
  }
  orders_detail.init({
    order_id: DataTypes.INTEGER,
    item_id: DataTypes.STRING,
    jumlah_pesanan: DataTypes.INTEGER,
    sub_total: DataTypes.NUMERIC
  }, {
    sequelize,
    modelName: 'orders_detail',
  });
  orders_detail.removeAttribute('id');
  return orders_detail;
};