'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('orders', {
      order_id: {
        type: Sequelize.STRING
      },
      id_user: {
        type: Sequelize.INTEGER
      },

      total_harga: {
        type: Sequelize.NUMERIC
      },
      tanggal_pesanan: {
        type: Sequelize.DATE
      },
      status_pesanan: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('orders');
  }
};