'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('orders_details', {
      
      order_id: {
        type: Sequelize.STRING
      },
      item_id: {
        type: Sequelize.INTEGER
      },
      jumlah_pesanan: {
        type: Sequelize.INTEGER
      },
      sub_total: {
        type: Sequelize.NUMERIC
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('orders_details');
  }
};