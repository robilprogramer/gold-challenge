const express = require("express")
const router = express.Router()
const { items } = require("../models");

//get semua data
router.get('/', async (req, res) => {
    const itemList = await items.findAll();
    return res.json({
        error: false,
        data: itemList,
    })
})

//get data by pk
router.get('/:id', async (req, res) => {
    const itemList = await items.findByPk(req.params.id);
    return res.json({
        error: false,
        data: itemList,
    })
})
//tambah items
router.post("/", async (req, res) => {
    const { kategori, nama, harga, stok, deskripsi } = req.body;
    const item = { kategori, nama, harga, stok, deskripsi };
    await items.create(item);
    res.status(201).json(item);
});

//update items
router.put('/:id', async (req, res) => {
    try {
        const item = await items.findByPk(req.params.id);
        const { kategori, nama, harga, stok, deskripsi } = req.body;
        const query = { where: { id: item.id } }

        if (item) {
            items.update({
                kategori: kategori,
                nama: nama,
                harga: harga,
                stok: stok,
                deskripsi: deskripsi
            }, query).then(() => {
                return res.status(200).json({
                    error: false,
                    message: `Items Dengan ID :  ${req.params.id} Berhasil Diubah`
                })
            }).catch(err => {
                return res.json({
                    data: err.message
                })
            })
        } else {
            return res.status(400).json({
                error: true,
                message: `Items Dengan ID :  ${req.params.id} tidak ditemukan`
            })
        }
    }
    catch (e) {
        return res.status(400).json({
            error: false,
            message: e.message
        })
    }
    
})

//hapus items
router.delete("/:id", async (req, res) => {
    
    const item = await items.findByPk(req.params.id);
    if (item) {
        items.destroy({where: {id: item.id } })
        .then(() => {
            return res.status(200).json({
                error: false,
                message: `Items Dengan ID :  ${req.params.id} Berhasil Dihapus`
            })}).catch(err => {return res.json({data: err.message})})}
    else
    {
        return res.status(400).json({
        error: true,
        message: `Items Dengan ID :  ${req.params.id} tidak ditemukan`
    })}
});


module.exports = router