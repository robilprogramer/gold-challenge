// Impor modul ekspres
const express = require("express")
const jwt = require("jsonwebtoken");
const router = express.Router()
const { users } = require("../models");
const ValidasiRegister = require('../Middleware/RegisterValidasi')

//Menangani permintaan menggunakan route
router.post("/login", async (req, res) => {

    const { email, password } = req.body;
    const isFoundUser = await users.findOne({ where: { email } });
  

    if (isFoundUser) {
        const isValidPassword = isFoundUser.password == password;
        const namaLengkap = isFoundUser.namaLengkap;
        if (isValidPassword) {
            const jwtPayload = jwt.sign(
                {
                    id: isFoundUser.id,
                    email: isFoundUser.email,
                    namaLengkap: isFoundUser.namaLengkap
                }, "Robil12345!@#$5"
            );
            return res.json({
                token: jwtPayload,
                message: "Login Berhasil",
                NamaLengkap: namaLengkap
            });
        }
    }

    return res.status(400).json({
        error: true,
        message: "Login gagal, user tidak terdaftar atau password anda salah",
    });
});


router.post("/daftar", ValidasiRegister,async (req, res) => {
    

    const { email, nama_lengkap, nomor_telepon, password, jenis_kelamin, tanggal_lahir, alamat } = req.body;
    
    // Validasi User Sudah Terdaftar

    const isUserExistByemail = await users.findOne({ where: { email } });
    const isUserExistBynotelpon = await users.findOne({ where: { nomor_telepon } });

    if (isUserExistByemail || isUserExistBynotelpon ) {
        return res.send({
            message: "Email / Nomor Telpon Sudah Terdaftar!"
        });
    }
    //set varibel user
    const user = { email, nama_lengkap, nomor_telepon, password, jenis_kelamin, tanggal_lahir, alamat };
    
    await users.create(user);

    res.status(201).json(user);
});

// Mengimpor router
module.exports = router