const express = require("express")
const router = express.Router()
const { orders, orders_detail } = require("../models");
const isAuthenticated = require("../Middleware/Authentication");


let obj = {};
let orderDetail ={};

//Lihat Semua Pesanan
router.get("/", isAuthenticated, async (req, res) => {    
    const ordersList = await orders.findAll();
    return res.json({
        error: false,
        data: ordersList,
    })
});

//Lihat Pesanan By ID
router.get("/:id", isAuthenticated, async (req, res) => {
    let id = req.params.id
    const ordersList = await orders.findOne({ where: { id } });
    return res.json({
        error: false,
        data: ordersList,
    })
});

//Membuat Pesanan Dengan Auth
router.post("/", isAuthenticated, async (req, res) => {
    const { total_harga, tanggal_pesanan, detail_pesanan } = req.body;
    
    let id_user = req.userData.id;
    let status_pesanan = "DALAM PROSES";
    const order = { id_user, total_harga, tanggal_pesanan, status_pesanan};
    const DataOrder =  await orders.create(order);
    
    obj = detail_pesanan;
    for ( i = 0; i < obj.length; i++) {
        let order_id = DataOrder.id;
        let item_id = obj[i].item_id;
        let jumlah_pesanan = obj[i].jumlah_pesanan;
        let sub_total = obj[i].sub_total;
         orderDetail = { 
             order_id: order_id,
             item_id: item_id,
             jumlah_pesanan: jumlah_pesanan,
             sub_total: sub_total }
        await orders_detail.create(orderDetail);
    }
    res.status(201).json("Pesan Sukses Dibuat");
});

//Ubah Status Pesanan
router.put('/:id', async (req, res) => {
    let id = req.params.id
    const order = await orders.findOne({ where: { id } });
   
    const { status_pesanan } = req.body;
    const query = { where: { id: order.id } }

    if (order) {
        orders.update({
            status_pesanan: status_pesanan
        }, query).then(() => {
            return res.status(200).json({
                error: false,
                message: `Status Pesanan Dengan Order ID :  ${req.params.id} Berhasil Diubah`
            })
        }).catch(err => {
            return res.json({
                data: err.message
            })
        })
    } else {
        return res.status(400).json({
            error: true,
            message: `Order ID :  ${req.params.id} tidak ditemukan`
        })
    }
})

module.exports = router