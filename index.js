const express = require("express")
const PenggunaRouter = require("./routes/Pengguna")
const ItemRouter = require("./routes/Items")
const OrderRouter = require("./routes/order")
const app = express()
const port = 1997

//parse
app.use(express.json());

app.get("/", (req, res) => {
    return res.send("Selamat Datang, Server e-commerce || Express JS");
});

//Pengguna
app.use("/pengguna", PenggunaRouter)
//items
app.use("/items", ItemRouter)
//order
app.use("/orders", OrderRouter)

//SERVER APLIKASI
app.listen(1997, () => console.log("Aplikasi Berjalan Pada Port '"+ port +"'"));
