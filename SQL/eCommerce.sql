PGDMP          )            	    z         	   eCommerce    13.3    13.3     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    32879 	   eCommerce    DATABASE     o   CREATE DATABASE "eCommerce" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'English_United States.1252';
    DROP DATABASE "eCommerce";
                postgres    false            �            1259    32880    SequelizeMeta    TABLE     R   CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);
 #   DROP TABLE public."SequelizeMeta";
       public         heap    postgres    false            �            1259    32898    items    TABLE       CREATE TABLE public.items (
    id integer NOT NULL,
    kategori character varying(255),
    nama character varying(255),
    harga numeric,
    stok integer,
    deskripsi text,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public.items;
       public         heap    postgres    false            �            1259    32896    items_id_seq    SEQUENCE     �   CREATE SEQUENCE public.items_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.items_id_seq;
       public          postgres    false    204            �           0    0    items_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.items_id_seq OWNED BY public.items.id;
          public          postgres    false    203            �            1259    32938    orders    TABLE     4  CREATE TABLE public.orders (
    id integer NOT NULL,
    id_user character varying(255),
    total_harga numeric,
    tanggal_pesanan timestamp with time zone,
    status_pesanan character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public.orders;
       public         heap    postgres    false            �            1259    32918    orders_details    TABLE     �   CREATE TABLE public.orders_details (
    order_id integer,
    item_id integer,
    jumlah_pesanan integer,
    sub_total numeric,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
 "   DROP TABLE public.orders_details;
       public         heap    postgres    false            �            1259    32936    orders_id_seq    SEQUENCE     �   CREATE SEQUENCE public.orders_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.orders_id_seq;
       public          postgres    false    207            �           0    0    orders_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.orders_id_seq OWNED BY public.orders.id;
          public          postgres    false    206            �            1259    32887    users    TABLE     �  CREATE TABLE public.users (
    id integer NOT NULL,
    email character varying(255),
    nama_lengkap character varying(255),
    nomor_telepon character varying(255),
    password character varying(255),
    jenis_kelamin character varying(255),
    tanggal_lahir timestamp with time zone,
    alamat character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    32885    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public          postgres    false    202            �           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
          public          postgres    false    201            ;           2604    32901    items id    DEFAULT     d   ALTER TABLE ONLY public.items ALTER COLUMN id SET DEFAULT nextval('public.items_id_seq'::regclass);
 7   ALTER TABLE public.items ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    204    204            <           2604    32941 	   orders id    DEFAULT     f   ALTER TABLE ONLY public.orders ALTER COLUMN id SET DEFAULT nextval('public.orders_id_seq'::regclass);
 8   ALTER TABLE public.orders ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    206    207            :           2604    32890    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    201    202    202            �          0    32880    SequelizeMeta 
   TABLE DATA           /   COPY public."SequelizeMeta" (name) FROM stdin;
    public          postgres    false    200   N!       �          0    32898    items 
   TABLE DATA           e   COPY public.items (id, kategori, nama, harga, stok, deskripsi, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    204   �!       �          0    32938    orders 
   TABLE DATA           u   COPY public.orders (id, id_user, total_harga, tanggal_pesanan, status_pesanan, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    207   z"       �          0    32918    orders_details 
   TABLE DATA           p   COPY public.orders_details (order_id, item_id, jumlah_pesanan, sub_total, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    205   #       �          0    32887    users 
   TABLE DATA           �   COPY public.users (id, email, nama_lengkap, nomor_telepon, password, jenis_kelamin, tanggal_lahir, alamat, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    202   p#       �           0    0    items_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.items_id_seq', 8, true);
          public          postgres    false    203            �           0    0    orders_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.orders_id_seq', 46, true);
          public          postgres    false    206            �           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 6, true);
          public          postgres    false    201            >           2606    32884     SequelizeMeta SequelizeMeta_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);
 N   ALTER TABLE ONLY public."SequelizeMeta" DROP CONSTRAINT "SequelizeMeta_pkey";
       public            postgres    false    200            B           2606    32906    items items_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.items
    ADD CONSTRAINT items_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.items DROP CONSTRAINT items_pkey;
       public            postgres    false    204            D           2606    32946    orders orders_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.orders
    ADD CONSTRAINT orders_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.orders DROP CONSTRAINT orders_pkey;
       public            postgres    false    207            @           2606    32895    users users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public            postgres    false    202            �   ^   x^u��
� ���b��ڻt�܃Qj��)����?�`&���Go����=��2��pc5NU�/�ஏ����n�֐��	yL�3$B����s�09�      �   �   x^��;�0D��SУX���5W�1RE���/B��"L��7;��Ϸ�����7�<��9�v�x��؁�H�To�'�I�x��T�@h�B"5!��B��P'�I�a���a�� ���&덒-_�_�`<����Q;=�x�|�ԕ����5&�&r�$x0�x���o      �   z   x^31�4�42  md�kh�k`�`hfe`bel�gjf�m`���������Pg�`hne`fej�gah	R7C��Č8��KS3�+�Ѝ3�21�3���&+#3=##�W� #�*�      �   \   x^}��� k{��98��80K��#!�~�$8)�R��,�$�C�K�.���?�ᯮ`��[F��GG�h+B�`���.�Ł	��DU//      �   �   x^�ҽj1�Z������IU�@!���9ǎ�I�?�*r|���@�Ш��]���u���N�q�o�ν�ݳё~̊1��a��i���d�ˠ�l5Z�AsU=��������PI���"�X\"��G��e�u�iA�E�/dX�4���8.��H�%���R�<P]�H���+޿h,U�+��.�Թ���b�K�a�T����ŉ����f     